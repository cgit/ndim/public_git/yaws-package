Name:		yaws
Version:	1.80
Release:	1%{?dist}
Summary:	FIXME

Group:		System Environment/Daemons
License:	MIT/BSD/FIXME
URL:		http://yaws.hyber.org/
Source0:	http://yaws.hyber.org/download/%{name}-%{version}.tar.gz
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	erlang
Requires:	%{name}-doc = %{version}-%{release}
Requires:	%{name}-wwwdata = %{version}-%{release}

%description
FIXME


%package devel
Summary:	FIXME

%description devel
FIXME


%package doc
Summary:	FIXME
BuildArch:	noarch

%description doc
FIXME


%package wwwdata
Summary:	FIXME
BuildArch:	noarch

%description wwwdata
FIXME


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}
# Making docs does not work.
# make %{?_smp_mflags} docs


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT%{_localstatedir}/yaws -type f -exec chmod -x {} \;
rm -f $RPM_BUILD_ROOT%{_libdir}/yaws/ebin/.empty


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README LICENSE
%{_sysconfdir}/init.d/yaws
%config(noreplace) %{_sysconfdir}/yaws-cert.pem
%config(noreplace) %{_sysconfdir}/yaws-key.pem
%config(noreplace) %{_sysconfdir}/yaws.conf
%dir %{_localstatedir}/log/%{name}
%{_bindir}/yaws
%{_libdir}/yaws/ebin/authmod_gssapi.beam
%{_libdir}/yaws/ebin/haxe.beam
%{_libdir}/yaws/ebin/json.beam
%{_libdir}/yaws/ebin/jsonrpc.beam
%{_libdir}/yaws/ebin/mime_type_c.beam
%{_libdir}/yaws/ebin/mime_types.beam
%{_libdir}/yaws/ebin/yaws.app
%{_libdir}/yaws/ebin/yaws.beam
%{_libdir}/yaws/ebin/yaws_404.beam
%{_libdir}/yaws/ebin/yaws_api.beam
%{_libdir}/yaws/ebin/yaws_app.beam
%{_libdir}/yaws/ebin/yaws_appmod_cgi.beam
%{_libdir}/yaws/ebin/yaws_cgi.beam
%{_libdir}/yaws/ebin/yaws_compile.beam
%{_libdir}/yaws/ebin/yaws_config.beam
%{_libdir}/yaws/ebin/yaws_ctl.beam
%{_libdir}/yaws/ebin/yaws_dav.beam
%{_libdir}/yaws/ebin/yaws_debug.beam
%{_libdir}/yaws/ebin/yaws_generated.beam
%{_libdir}/yaws/ebin/yaws_html.beam
%{_libdir}/yaws/ebin/yaws_jsonrpc.beam
%{_libdir}/yaws/ebin/yaws_log.beam
%{_libdir}/yaws/ebin/yaws_log_file_h.beam
%{_libdir}/yaws/ebin/yaws_ls.beam
%{_libdir}/yaws/ebin/yaws_pam.beam
%{_libdir}/yaws/ebin/yaws_revproxy.beam
%{_libdir}/yaws/ebin/yaws_rpc.beam
%{_libdir}/yaws/ebin/yaws_rss.beam
%{_libdir}/yaws/ebin/yaws_sendfile.beam
%{_libdir}/yaws/ebin/yaws_sendfile_compat.beam
%{_libdir}/yaws/ebin/yaws_server.beam
%{_libdir}/yaws/ebin/yaws_session_server.beam
%{_libdir}/yaws/ebin/yaws_soap_lib.beam
%{_libdir}/yaws/ebin/yaws_soap_srv.beam
%{_libdir}/yaws/ebin/yaws_sup.beam
%{_libdir}/yaws/ebin/yaws_sup_restarts.beam
%{_libdir}/yaws/ebin/yaws_ticker.beam
%{_libdir}/yaws/ebin/yaws_xmlrpc.beam
%{_libdir}/yaws/ebin/yaws_zlib.beam
%{_libdir}/yaws/include/erlsom.hrl
%{_libdir}/yaws/include/soap.hrl
%{_libdir}/yaws/include/yaws.hrl
%{_libdir}/yaws/include/yaws_api.hrl
%{_libdir}/yaws/include/yaws_dav.hrl
%{_libdir}/yaws/priv/envelope.xsd
%{_libdir}/yaws/priv/lib/epam
%{_libdir}/yaws/priv/lib/setuid_drv.so
%{_libdir}/yaws/priv/lib/yaws_sendfile_drv.so
%{_libdir}/yaws/priv/soap.xsd
%{_libdir}/yaws/priv/wsdl.xsd

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/yaws.pc

%files doc
%defattr(-,root,root,-)
%doc doc/yaws.pdf
%doc %{_datadir}/man/man1/yaws.1.gz
%doc %{_datadir}/man/man5/yaws.conf.5.gz
%doc %{_datadir}/man/man5/yaws_api.5.gz

%files wwwdata
%defattr(-,root,root,-)
%{_localstatedir}/yaws/ebin/myappmod.beam
%{_localstatedir}/yaws/ebin/shopcart.beam
%{_localstatedir}/yaws/www/END
%{_localstatedir}/yaws/www/END2
%{_localstatedir}/yaws/www/HEAD
%{_localstatedir}/yaws/www/TAB.inc
%{_localstatedir}/yaws/www/api.yaws
%{_localstatedir}/yaws/www/appmods.yaws
%{_localstatedir}/yaws/www/arg.yaws
%{_localstatedir}/yaws/www/arg2.yaws
%{_localstatedir}/yaws/www/bindings.yaws
%{_localstatedir}/yaws/www/cgi-bin/foo.py
%{_localstatedir}/yaws/www/cgi-bin/foo.pyc
%{_localstatedir}/yaws/www/cgi-bin/foo.pyo
%{_localstatedir}/yaws/www/cgi.yaws
%{_localstatedir}/yaws/www/code.yaws
%{_localstatedir}/yaws/www/code/Makefile
%{_localstatedir}/yaws/www/code/myappmod.beam
%{_localstatedir}/yaws/www/code/myappmod.erl
%{_localstatedir}/yaws/www/compile_layout.dia
%{_localstatedir}/yaws/www/compile_layout.png
%{_localstatedir}/yaws/www/configuration.yaws
%{_localstatedir}/yaws/www/contact.yaws
%{_localstatedir}/yaws/www/contribs.yaws
%{_localstatedir}/yaws/www/cookies.yaws
%{_localstatedir}/yaws/www/doc.yaws
%{_localstatedir}/yaws/www/dynamic.yaws
%{_localstatedir}/yaws/www/embed.yaws
%{_localstatedir}/yaws/www/examples.yaws
%{_localstatedir}/yaws/www/favicon.ico
%{_localstatedir}/yaws/www/form.yaws
%{_localstatedir}/yaws/www/haxe_intro.yaws
%{_localstatedir}/yaws/www/haxe_sample.html
%{_localstatedir}/yaws/www/haxe_sample.yaws
%{_localstatedir}/yaws/www/history.yaws
%{_localstatedir}/yaws/www/icons/README
%{_localstatedir}/yaws/www/icons/a.gif
%{_localstatedir}/yaws/www/icons/alert.black.gif
%{_localstatedir}/yaws/www/icons/alert.red.gif
%{_localstatedir}/yaws/www/icons/apache_pb.gif
%{_localstatedir}/yaws/www/icons/back.gif
%{_localstatedir}/yaws/www/icons/ball.gray.gif
%{_localstatedir}/yaws/www/icons/ball.red.gif
%{_localstatedir}/yaws/www/icons/binary.gif
%{_localstatedir}/yaws/www/icons/binhex.gif
%{_localstatedir}/yaws/www/icons/blank.gif
%{_localstatedir}/yaws/www/icons/bomb.gif
%{_localstatedir}/yaws/www/icons/box1.gif
%{_localstatedir}/yaws/www/icons/box2.gif
%{_localstatedir}/yaws/www/icons/broken.gif
%{_localstatedir}/yaws/www/icons/burst.gif
%{_localstatedir}/yaws/www/icons/c.gif
%{_localstatedir}/yaws/www/icons/comp.blue.gif
%{_localstatedir}/yaws/www/icons/comp.gray.gif
%{_localstatedir}/yaws/www/icons/compressed.gif
%{_localstatedir}/yaws/www/icons/continued.gif
%{_localstatedir}/yaws/www/icons/dir.gif
%{_localstatedir}/yaws/www/icons/down.gif
%{_localstatedir}/yaws/www/icons/dvi.gif
%{_localstatedir}/yaws/www/icons/erl.gif
%{_localstatedir}/yaws/www/icons/f.gif
%{_localstatedir}/yaws/www/icons/folder.gif
%{_localstatedir}/yaws/www/icons/folder.open.gif
%{_localstatedir}/yaws/www/icons/folder.sec.gif
%{_localstatedir}/yaws/www/icons/forward.gif
%{_localstatedir}/yaws/www/icons/generic.gif
%{_localstatedir}/yaws/www/icons/generic.red.gif
%{_localstatedir}/yaws/www/icons/generic.sec.gif
%{_localstatedir}/yaws/www/icons/gnu-head-tiny.jpg
%{_localstatedir}/yaws/www/icons/hand.right.gif
%{_localstatedir}/yaws/www/icons/hand.up.gif
%{_localstatedir}/yaws/www/icons/hrl.gif
%{_localstatedir}/yaws/www/icons/icon.sheet.gif
%{_localstatedir}/yaws/www/icons/image1.gif
%{_localstatedir}/yaws/www/icons/image2.gif
%{_localstatedir}/yaws/www/icons/image3.gif
%{_localstatedir}/yaws/www/icons/index.gif
%{_localstatedir}/yaws/www/icons/layout.gif
%{_localstatedir}/yaws/www/icons/left.gif
%{_localstatedir}/yaws/www/icons/link.gif
%{_localstatedir}/yaws/www/icons/mailman-large.jpg
%{_localstatedir}/yaws/www/icons/mailman.jpg
%{_localstatedir}/yaws/www/icons/movie.gif
%{_localstatedir}/yaws/www/icons/p.gif
%{_localstatedir}/yaws/www/icons/patch.gif
%{_localstatedir}/yaws/www/icons/pdf.gif
%{_localstatedir}/yaws/www/icons/php4.gif
%{_localstatedir}/yaws/www/icons/pie0.gif
%{_localstatedir}/yaws/www/icons/pie1.gif
%{_localstatedir}/yaws/www/icons/pie2.gif
%{_localstatedir}/yaws/www/icons/pie3.gif
%{_localstatedir}/yaws/www/icons/pie4.gif
%{_localstatedir}/yaws/www/icons/pie5.gif
%{_localstatedir}/yaws/www/icons/pie6.gif
%{_localstatedir}/yaws/www/icons/pie7.gif
%{_localstatedir}/yaws/www/icons/pie8.gif
%{_localstatedir}/yaws/www/icons/portal.gif
%{_localstatedir}/yaws/www/icons/ps.gif
%{_localstatedir}/yaws/www/icons/quill.gif
%{_localstatedir}/yaws/www/icons/right.gif
%{_localstatedir}/yaws/www/icons/screw1.gif
%{_localstatedir}/yaws/www/icons/screw2.gif
%{_localstatedir}/yaws/www/icons/script.gif
%{_localstatedir}/yaws/www/icons/small/README.txt
%{_localstatedir}/yaws/www/icons/small/back.gif
%{_localstatedir}/yaws/www/icons/small/binary.gif
%{_localstatedir}/yaws/www/icons/small/binhex.gif
%{_localstatedir}/yaws/www/icons/small/blank.gif
%{_localstatedir}/yaws/www/icons/small/broken.gif
%{_localstatedir}/yaws/www/icons/small/burst.gif
%{_localstatedir}/yaws/www/icons/small/comp1.gif
%{_localstatedir}/yaws/www/icons/small/comp2.gif
%{_localstatedir}/yaws/www/icons/small/compressed.gif
%{_localstatedir}/yaws/www/icons/small/continued.gif
%{_localstatedir}/yaws/www/icons/small/dir.gif
%{_localstatedir}/yaws/www/icons/small/dir2.gif
%{_localstatedir}/yaws/www/icons/small/doc.gif
%{_localstatedir}/yaws/www/icons/small/forward.gif
%{_localstatedir}/yaws/www/icons/small/generic.gif
%{_localstatedir}/yaws/www/icons/small/generic2.gif
%{_localstatedir}/yaws/www/icons/small/generic3.gif
%{_localstatedir}/yaws/www/icons/small/image.gif
%{_localstatedir}/yaws/www/icons/small/image2.gif
%{_localstatedir}/yaws/www/icons/small/index.gif
%{_localstatedir}/yaws/www/icons/small/key.gif
%{_localstatedir}/yaws/www/icons/small/movie.gif
%{_localstatedir}/yaws/www/icons/small/patch.gif
%{_localstatedir}/yaws/www/icons/small/ps.gif
%{_localstatedir}/yaws/www/icons/small/rainbow.gif
%{_localstatedir}/yaws/www/icons/small/sound.gif
%{_localstatedir}/yaws/www/icons/small/sound2.gif
%{_localstatedir}/yaws/www/icons/small/tar.gif
%{_localstatedir}/yaws/www/icons/small/text.gif
%{_localstatedir}/yaws/www/icons/small/transfer.gif
%{_localstatedir}/yaws/www/icons/small/unknown.gif
%{_localstatedir}/yaws/www/icons/small/uu.gif
%{_localstatedir}/yaws/www/icons/sound1.gif
%{_localstatedir}/yaws/www/icons/sound2.gif
%{_localstatedir}/yaws/www/icons/sphere1.gif
%{_localstatedir}/yaws/www/icons/sphere2.gif
%{_localstatedir}/yaws/www/icons/tar.gif
%{_localstatedir}/yaws/www/icons/tex.gif
%{_localstatedir}/yaws/www/icons/text.gif
%{_localstatedir}/yaws/www/icons/transfer.gif
%{_localstatedir}/yaws/www/icons/unknown.gif
%{_localstatedir}/yaws/www/icons/up.gif
%{_localstatedir}/yaws/www/icons/uu.gif
%{_localstatedir}/yaws/www/icons/uuencoded.gif
%{_localstatedir}/yaws/www/icons/world1.gif
%{_localstatedir}/yaws/www/icons/world2.gif
%{_localstatedir}/yaws/www/icons/yaws.gif
%{_localstatedir}/yaws/www/icons/yawsY.gif
%{_localstatedir}/yaws/www/icons/yaws_head.gif
%{_localstatedir}/yaws/www/icons/yaws_pb.gif
%{_localstatedir}/yaws/www/icons/yaws_y.gif
%{_localstatedir}/yaws/www/index.yaws
%{_localstatedir}/yaws/www/internals.yaws
%{_localstatedir}/yaws/www/jsolait/jsolait.js
%{_localstatedir}/yaws/www/jsolait/lib/jsonrpc.js
%{_localstatedir}/yaws/www/jsolait/lib/urllib.js
%{_localstatedir}/yaws/www/json_intro.yaws
%{_localstatedir}/yaws/www/json_sample.html
%{_localstatedir}/yaws/www/json_sample.yaws
%{_localstatedir}/yaws/www/man.yaws
%{_localstatedir}/yaws/www/motivation.yaws
%{_localstatedir}/yaws/www/news
%{_localstatedir}/yaws/www/pcookie.yaws
%{_localstatedir}/yaws/www/post.yaws
%{_localstatedir}/yaws/www/privbind.yaws
%{_localstatedir}/yaws/www/process_tree.dia
%{_localstatedir}/yaws/www/process_tree.png
%{_localstatedir}/yaws/www/query.yaws
%{_localstatedir}/yaws/www/readcookie.yaws
%{_localstatedir}/yaws/www/readpcookie.yaws
%{_localstatedir}/yaws/www/redirect.yaws
%{_localstatedir}/yaws/www/redirect2.yaws
%{_localstatedir}/yaws/www/redirect3.yaws
%{_localstatedir}/yaws/www/redirect4.yaws
%{_localstatedir}/yaws/www/session.yaws
%{_localstatedir}/yaws/www/session1.yaws
%{_localstatedir}/yaws/www/setcookie.yaws
%{_localstatedir}/yaws/www/setpcookie.yaws
%{_localstatedir}/yaws/www/shopingcart/Makefile
%{_localstatedir}/yaws/www/shopingcart/buy.yaws
%{_localstatedir}/yaws/www/shopingcart/index.yaws
%{_localstatedir}/yaws/www/shopingcart/junk.jpg
%{_localstatedir}/yaws/www/shopingcart/loginpost.yaws
%{_localstatedir}/yaws/www/shopingcart/logout.yaws
%{_localstatedir}/yaws/www/shopingcart/shopcart.beam
%{_localstatedir}/yaws/www/shopingcart/shopcart.erl
%{_localstatedir}/yaws/www/shopingcart/shopcart_form.yaws
%{_localstatedir}/yaws/www/shopingcart/source.html
%{_localstatedir}/yaws/www/shopingcart/style.css
%{_localstatedir}/yaws/www/simple.yaws
%{_localstatedir}/yaws/www/simple_ex1.yaws
%{_localstatedir}/yaws/www/simple_ex2.yaws
%{_localstatedir}/yaws/www/simple_ex3.yaws
%{_localstatedir}/yaws/www/simple_ex4.yaws
%{_localstatedir}/yaws/www/small.yaws
%{_localstatedir}/yaws/www/soap_intro.yaws
%{_localstatedir}/yaws/www/spacer.gif
%{_localstatedir}/yaws/www/ssi.yaws
%{_localstatedir}/yaws/www/ssi/dynamic.1
%{_localstatedir}/yaws/www/ssi/dynamic.2
%{_localstatedir}/yaws/www/ssi/dynamic.3
%{_localstatedir}/yaws/www/ssi_ex1
%{_localstatedir}/yaws/www/static.html
%{_localstatedir}/yaws/www/stats.yaws
%{_localstatedir}/yaws/www/stats_ex.yaws
%{_localstatedir}/yaws/www/stil.css
%{_localstatedir}/yaws/www/stream.yaws
%{_localstatedir}/yaws/www/testdir/index.html
%{_localstatedir}/yaws/www/todo.yaws
%{_localstatedir}/yaws/www/upload.yaws
%{_localstatedir}/yaws/www/upload0.yaws
%{_localstatedir}/yaws/www/urandom.yaws
%{_localstatedir}/yaws/www/yapp_intro.yaws
%{_localstatedir}/yaws/www/yaws-1.55_to_1.56.patch
%{_localstatedir}/yaws/www/ybed.erl
%{_localstatedir}/yaws/www/yman.yaws


%changelog
* Thu Mar  5 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 1.80-1
- Initial package

